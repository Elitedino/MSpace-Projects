#like 6.2 follow the diagram
import RPi.GPIO as GPIO
from time import sleep

GPIO.setmode(GPIO.BCM)
GPIO.setup(25, GPIO.IN)
GPIO.setup(24, GPIO.OUT)

try:
    while True:
        if GPIO.input(25):
            print("Port 25 is 1/HIGH/True - LED ON")
            GPIO.output(24, 1)
        else:
            print("Port 25 is 0/LOW/False - LED OFF")
            GPIO.output(24, 0)
        sleep(0.1)

finally:
    GPIO.cleanup()
