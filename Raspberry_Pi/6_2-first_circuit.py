#follow diagram on project page, https://docs.google.com/document/d/1kkuuFGoSCsdgyhLenx0ka7LK8-JvIFl8bSnotFtNsnw/edit
#this file is just the code for said circuit
from gpiozero import LED
from time import sleep
led = LED(17)

while True:
    led.on()
    sleep(1)
    led.off()
    sleep(1)
